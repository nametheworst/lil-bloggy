---
widget: starter.blog.intro
widget_id: wah
headless: true
weight: 10
title: Welcome to my Blog!
subtitle: I'm Emilia, and I finally made my site look nice
active: true
design:
  background:
    color: "#090a0b"
    text_color_light: true
    image: apples.png
---
Heres links to my [Twitter](https://twitter.com/NameThatsIt1), my [Youtube channel](https://www.youtube.com/channel/UCxovZ61hE96VyYJvYJSQ97A), and my [osu! profile](https://osu.ppy.sh/users/8122104)


